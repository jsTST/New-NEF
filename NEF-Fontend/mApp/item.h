//
// Created by huangyang on 18-3-15.
//

#ifndef IRRTEST_ITEM_H
#define IRRTEST_ITEM_H

#include "irrlicht.h"
using namespace irr;

typedef enum item_type_e{
    MOD,
    TEX,
    IMG
} item_type_t;

class item {
public:
    int id;
    bool existence;
    bool visitable;
    item_type_t type;
    scene::ISceneNode* node;
    gui::IGUIStaticText * text;
    gui::IGUIImage * image;
    bool clicked;
    bool noticed;
    double posture[9];
};

class appItem {
public:
    int id;
    char name[512];
    char exep[512];
};


#endif //IRRTEST_ITEM_H
