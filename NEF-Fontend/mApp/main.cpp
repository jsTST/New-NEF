#include <iostream>

#include "irrContex.h"
#include "util.h"

int main() {
    irrContex * irr = new irrContex(800,480, false);

    addAxis(irr);
    addMouse(irr);
    pressMouse(irr,0);

    irr->setCamPos(50,50,50);

    //int m = irr->addItem(IMG,"../media/pics/wel_t.png",0,0);
    int i = 0;
    while(irr->drawScene()==0){
        hideMouse(irr);
        int cl = irr->refleshClick();
        irr->setItemVisitable(cl,(cl<0));
        if(i<180) i++;
        if(i == 180){
            i = -180;
            //delAxis(irr);
            if(irr->device->isFullscreen())
                return 0;
        }
        irr->setCamRot(0,i,0);
        showMouse(irr);
    }
    return 0;
}