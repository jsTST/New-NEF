//
// Created by huangyang on 18-3-29.
//

#ifndef MATCHAPPS_UTIL_H
#define MATCHAPPS_UTIL_H

#include "irrContex.h"
#include <vector>

#define mos_l 40

std::vector<int> axis;
int mouseId,mousePre;
int mousePress[4];

void addAxis(irrContex * irr){
    axis.clear();
    for(int i = -10;i<10;i++){
        int id;
        id = irr->addItem(MOD,"../media/objs/cube.obj");
        irr->setItemPos(id,i*10,0,0);
        axis.push_back(id);
        id = irr->addItem(MOD,"../media/objs/sphere.obj");
        irr->setItemPos(id,0,i*10,0);
        axis.push_back(id);
        id = irr->addItem(MOD,"../media/objs/cone.obj");
        irr->setItemPos(id,0,0,i*10);
        axis.push_back(id);
    }
}

void delAxis(irrContex * irr){
    for(auto i = axis.begin();i != axis.end();i++){
        irr->delItem(*i.base());
    }
}

void addMouse(irrContex * irr){
    mouseId = irr->addItem(IMG,"../media/pics/mos_t.png",0,0);
    irr->setItemPos(mouseId,irr->s_w/2-mos_l/2,irr->s_h/2-mos_l/2);
    mousePress[0] = irr->addItem(IMG,"../media/pics/mos_t.png",0,0);
    irr->setItemPos(mousePress[0],irr->s_w/2-(int)(mos_l*1.5),irr->s_h/2-(int)(mos_l*0.5));
    mousePress[1] = irr->addItem(IMG,"../media/pics/mos_t.png",0,0);
    irr->setItemPos(mousePress[1],irr->s_w/2-(int)(mos_l*0.5),irr->s_h/2-(int)(mos_l*1.5));
    mousePress[2] = irr->addItem(IMG,"../media/pics/mos_t.png",0,0);
    irr->setItemPos(mousePress[2],irr->s_w/2+(int)(mos_l*0.5),irr->s_h/2-(int)(mos_l*0.5));
    mousePress[3] = irr->addItem(IMG,"../media/pics/mos_t.png",0,0);
    irr->setItemPos(mousePress[3],irr->s_w/2-(int)(mos_l*0.5),irr->s_h/2+(int)(mos_l*0.5));
    irr->setItemVisitable(mousePress[0], false);
    irr->setItemVisitable(mousePress[1], false);
    irr->setItemVisitable(mousePress[2], false);
    irr->setItemVisitable(mousePress[3], false);
    mousePre = 0;
}

void hideMouse(irrContex *irr){
    irr->setItemVisitable(mouseId, false);
    irr->setItemVisitable(mousePress[0], false);
    irr->setItemVisitable(mousePress[1], false);
    irr->setItemVisitable(mousePress[2], false);
    irr->setItemVisitable(mousePress[3], false);
}

void pressMouse(irrContex * irr,int pre){
    switch (pre){
        case 0:
            irr->setItemVisitable(mousePress[0], false);
            irr->setItemVisitable(mousePress[1], false);
            irr->setItemVisitable(mousePress[2], false);
            irr->setItemVisitable(mousePress[3], false);
            mousePre = 0;
            break;
        case 1:
            irr->setItemVisitable(mousePress[0], true);
            irr->setItemVisitable(mousePress[1], false);
            irr->setItemVisitable(mousePress[2], false);
            irr->setItemVisitable(mousePress[3], false);
            mousePre = 1;
            break;
        case 2:
            irr->setItemVisitable(mousePress[0], true);
            irr->setItemVisitable(mousePress[1], true);
            irr->setItemVisitable(mousePress[2], false);
            irr->setItemVisitable(mousePress[3], false);
            mousePre = 2;
            break;

        case 3:
            irr->setItemVisitable(mousePress[0], true);
            irr->setItemVisitable(mousePress[1], true);
            irr->setItemVisitable(mousePress[2], true);
            irr->setItemVisitable(mousePress[3], false);
            mousePre = 3;
            break;
        case 4:
            irr->setItemVisitable(mousePress[0], true);
            irr->setItemVisitable(mousePress[1], true);
            irr->setItemVisitable(mousePress[2], true);
            irr->setItemVisitable(mousePress[3], true);
            mousePre = 4;
            break;
        default:
            break;
    }
}

void showMouse(irrContex * irr){
    irr->setItemVisitable(mouseId, true);
    pressMouse(irr,mousePre);
}

#endif //MATCHAPPS_UTIL_H
