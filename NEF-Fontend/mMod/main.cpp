#include <iostream>
#include <unistd.h>

#include "irrContex.h"
#include "util.h"
#include "cam.h"

#define DEBUG

enum STAT{
    NONE,
    CUBE,
    SPHERE,
    CONE
};

int main() {
    irrContex * irr = new irrContex(800,480, false);

    int welLogo;
    int welName;
    int tipLogo;

    double cubePos[3] = {10,0,0};
    double spherePos[3] = {20,0,0};
    double conePos[3] = {30,0,0};
    double showPos[3] = {20,40,20};
    double statScl[3] = {2,2,2};
    double showScl[3] = {5,5,5};

    int lastClick = 0;
    time_t clickTime = 0;
    int ClickItem = 0;

    enum STAT Sta = NONE;

    addMouse(irr);
#ifndef DEBUG

    welLogo = irr->addItem(IMG, "../media/pics/wel_t.png", 0, 0);
    fReflesh(irr);
    sleep(3);
    irr->setItemVisitable(welLogo, false);

    welName = irr->addItem(IMG, "../media/pics/mod_t.png", 0, 0);
    fReflesh(irr);
    sleep(3);
    irr->setItemVisitable(welName, false);

    tipLogo = irr->addItem(IMG, "../media/pics/see_t.png", 0, 0);
    fReflesh(irr);
    sleep(3);

    pressMouse(irr,1);
    fReflesh(irr);
    sleep(1);
    pressMouse(irr,2);
    fReflesh(irr);
    sleep(1);
    pressMouse(irr,3);
    fReflesh(irr);
    sleep(1);
    pressMouse(irr,4);
    fReflesh(irr);
    sleep(3);
    pressMouse(irr,0);
    irr->setItemVisitable(tipLogo, false);
    fReflesh(irr);

#endif

    //addAxis(irr);
    camInit(irr);

    int cube = irr->addItem(MOD,"../media/objs/cube.obj");
    int sphere = irr->addItem(MOD,"../media/objs/sphere.obj");
    int cone = irr->addItem(MOD,"../media/objs/cone.obj");

    irr->setItemPos(cube,cubePos[0],cubePos[1],cubePos[2]);
    irr->setItemScale(cube,statScl[0],statScl[1],statScl[2]);
    irr->setItemPos(sphere,spherePos[0],spherePos[1],spherePos[2]);
    irr->setItemScale(sphere,statScl[0],statScl[1],statScl[2]);
    irr->setItemPos(cone,conePos[0],conePos[1],conePos[2]);
    irr->setItemScale(cone,statScl[0],statScl[1],statScl[2]);

    irr->setCamPos(60,60,60);

    int i = 0;

    Sta = NONE;

    while(irr->drawScene()==0){
        switch (Sta){
            case NONE:
                irr->setItemPos(cube,cubePos[0],cubePos[1],cubePos[2]);
                irr->setItemPos(sphere,spherePos[0],spherePos[1],spherePos[2]);
                irr->setItemPos(cone,conePos[0],conePos[1],conePos[2]);
                irr->setItemScale(cube,statScl[0],statScl[1],statScl[2]);
                irr->setItemScale(sphere,statScl[0],statScl[1],statScl[2]);
                irr->setItemScale(cone,statScl[0],statScl[1],statScl[2]);
                break;
            case CUBE:
                irr->setItemPos(cube,showPos[0],showPos[1],showPos[2]);
                irr->setItemPos(sphere,spherePos[0],spherePos[1],spherePos[2]);
                irr->setItemPos(cone,conePos[0],conePos[1],conePos[2]);
                irr->setItemScale(cube,showScl[0],showScl[1],showScl[2]);
                irr->setItemScale(sphere,statScl[0],statScl[1],statScl[2]);
                irr->setItemScale(cone,statScl[0],statScl[1],statScl[2]);
                break;
            case SPHERE:
                irr->setItemPos(cube,cubePos[0],cubePos[1],cubePos[2]);
                irr->setItemPos(sphere,showPos[0],showPos[1],showPos[2]);
                irr->setItemPos(cone,conePos[0],conePos[1],conePos[2]);
                irr->setItemScale(cube,statScl[0],statScl[1],statScl[2]);
                irr->setItemScale(sphere,showScl[0],showScl[1],showScl[2]);
                irr->setItemScale(cone,statScl[0],statScl[1],statScl[2]);
                break;
            case CONE:
                irr->setItemPos(cube,cubePos[0],cubePos[1],cubePos[2]);
                irr->setItemPos(sphere,spherePos[0],spherePos[1],spherePos[2]);
                irr->setItemPos(cone,showPos[0],showPos[1],showPos[2]);
                irr->setItemScale(cube,statScl[0],statScl[1],statScl[2]);
                irr->setItemScale(sphere,statScl[0],statScl[1],statScl[2]);
                irr->setItemScale(cone,showScl[0],showScl[1],showScl[2]);
                break;
        }

        int click = irr->refleshClick();
        if(irr->noClickFlag){
            ClickItem = 0;
            lastClick = 0;
            clickTime = 0;
            pressMouse(irr,0);
        } else {
                if (click > 0) {
                    if (click != lastClick) {
                        ClickItem = 0;
                        lastClick = click;
                        clickTime = time(NULL);
                        pressMouse(irr,0);
                        std::cout << "change last click " << lastClick << std::endl;
                    }
                }
                if (click < 0) {
                    if (lastClick == (-1) * click) {
                        ClickItem = 0;
                        lastClick = 0;
                        clickTime = 0;
                        pressMouse(irr,0);
                        std::cout << "lost last click " << std::endl;
                    }
                }

            if (lastClick != 0) {
                if ((time(NULL) - clickTime) < 5) {
                    pressMouse(irr, (int) (time(NULL) - clickTime));
                } else {
                    ClickItem = lastClick;
                    lastClick = 0;
                    clickTime = 0;
                    pressMouse(irr, 0);
                }
            }
        }

        if(ClickItem > 0){
            if(ClickItem == cube){
                Sta = CUBE;
            }
            if(ClickItem == sphere){
                Sta = SPHERE;
            }
            if(ClickItem == cone){
                Sta = CONE;
            }
        }
    }
    return 0;
}