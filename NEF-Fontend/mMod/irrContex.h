//
// Created by huangyang on 18-3-15.
//

#ifndef RENDERSERVER_IRRCONTEX_H
#define RENDERSERVER_IRRCONTEX_H

#include "irrlicht.h"
#include "item.h"
#include <vector>
using namespace irr;

class irrContex {
public:
    IrrlichtDevice *device;
    video::IVideoDriver* driver;
    scene::ISceneManager* smgr;
    gui::IGUIEnvironment* env;
    gui::IGUIStaticText* stext;
    //scene::ICameraSceneNode *mainCam;
    scene::ICameraSceneNode *mainCam;
    scene::ICameraSceneNode *cams[2];
    std::vector<item *> itemList;
    int new_id;
    int s_w,s_h;
    bool noClickFlag;
    core::stringw tmp;
public:
    irrContex(int width,int height,bool full);
    ~irrContex();
    int addItem(item_type_t item_type, const char *initData,int x1 = 0,int y1 =0,int x2 = 0,int y2 = 0);
    int addItem(int id,item_type_t item_type, const char *initData,int x1 = 0,int y1 =0,int x2 = 0,int y2 = 0);
    int delItem(int id);
    item * getItem(int id);
    int refleshClick();
    int setItemPos(int id,double x = 0,double y = 0,double z = 0);
    int setItemRot(int id,double x = 0,double y = 0,double z = 0);
    int setItemScale(int id,double x = 0,double y = 0,double z = 0);
    int setItemVisitable(int id,bool visitable = true);
    int setItemText(int id,const wchar_t * tex);
    int drawScene();
    int setCamPos(double x,double y,double z);
    int setCamRot(double x,double y,double z);
    int setCamTarget(double x,double y,double z);
    int setClick(int id,bool click){
        item * i = getItem(id);
        if (i != nullptr) {
            i->clicked = true;
            return 0;
        } else return -1;
    }
};


#endif //RENDERSERVER_IRRCONTEX_H
