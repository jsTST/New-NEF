//
// Created by huangyang on 18-3-29.
//

#ifndef MATCHAPPS_CAM_H
#define MATCHAPPS_CAM_H

#include "irrContex.h"

double campos[3];
double camrot[3];

void camInit(irrContex * irr){
    irr->cams[0]->setPosition(core::vector3df(0,0,0));
    irr->cams[0]->setUpVector(core::vector3df(0,1,0));
    irr->cams[0]->setTarget(core::vector3df(0,0,-1));
}

void camDataGet(){
    campos[0] = 60.0;
    campos[1] = 60.0;
    campos[2] = 60.0;
    camrot[0] = 0;
    camrot[1] = 0;
    camrot[2] = 0;
}

void camReflesh(irrContex * irr){
    irr->setCamPos(campos[0],campos[1],campos[2]);
    irr->setCamRot(camrot[0],camrot[1],camrot[2]);
}

#endif //MATCHAPPS_CAM_H
